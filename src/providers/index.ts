import { Provider } from "../core/provider";
import { DBProvider } from './dbProvider';
import { ServerProvider } from "./serverProvider";

const providers: Provider[] = [
  new DBProvider(),
  new ServerProvider(),
];

(async () => {
  for (const provider of providers) {
    await provider.run();
  }
})();
