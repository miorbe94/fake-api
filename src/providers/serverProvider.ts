import express, { Express } from 'express';
import morgan from 'morgan';
import cors from 'cors';
import helmet from 'helmet';

import { Provider } from "../core/provider";
import { PORT } from '../opts';
import routers from '../domain/router';
import { errorHandler } from '../errorHandler';

export class ServerProvider extends Provider {
  app: Express = express();

  middlewares = [
    express.urlencoded({ extended: false }),
    express.json(),
    morgan('dev'),
    cors(),
    helmet(),
  ];

  constructor() {
    super();
    this.middlewares.forEach((middleware) => {
      this.app.use(middleware);
    })
    routers.forEach((router) => {
      this.app.use(router);
    });
    this.app.use(errorHandler);
  }

  async run() {
    console.log('App listening on port', PORT);
    await this.app.listen(PORT);
  }
}
