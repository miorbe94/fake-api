import { createConnection } from 'typeorm';
import { Provider } from "../core/provider";

export class DBProvider extends Provider {
  async run() {
    await createConnection();
    console.log('DB connected');
  }
}
