import dotEnv from 'dotEnv';
dotEnv.config();
import "reflect-metadata";

/*** run all providers */
import './providers';
