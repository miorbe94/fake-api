import { Router } from 'express';
import { CustomRouter } from '../core/customRouter';
import { UserRouter } from './user/user.router';

const routers: CustomRouter[] = [
  new UserRouter(),
];

export default routers.map(({ name, routes }) => {
  const router = Router();
  routes.forEach(({ method, url, controller }) => {
    router[method](url, async (req, res, next) => {
      try {
        await controller(req, res);
      } catch (error) {
        next(error);
      }
    });
  });
  return Router().use(`/${name}`, router);
});