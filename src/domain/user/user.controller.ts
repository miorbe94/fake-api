import { Request, Response } from "express";

import * as userService from './user.service';

export const getAllUsers = async (req: Request, res: Response) => {
  return res.json(await userService.getAllUsers());
};

export const createUser = async (req: Request, res: Response) => {
  const { 
    firstName,
    lastName,
    email
   } = req.body;
   return res.json(await userService.createUser({ firstName, lastName, email }));
};