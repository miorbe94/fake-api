import { getRepository } from 'typeorm';
import { User } from '../../db/entity/User';

export const getAll = () => {
  const repository = getRepository(User);
  return repository.find();
};

export const create = (user: User) => {
  const repository = getRepository(User);
  return repository.save(user);
};
