import { CustomRouter } from "../../core/customRouter";
import * as controller from './user.controller';

export class UserRouter extends CustomRouter {
  name = 'user';

  routes = [
    {
      method: 'get',
      url: '/',
      controller: controller.getAllUsers,
    },
    {
      method: 'post',
      url: '/',
      controller: controller.createUser,
    },
  ];
}
