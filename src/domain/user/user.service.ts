import { User } from '../../db/entity/User';
import * as userRepository from './user.repository';

export const getAllUsers = () => {
  return userRepository.getAll();
};

export const createUser = (user: User) => {
  return userRepository.create(user);
};