import { Request, Response } from "express";

interface Route {
  method: string;
  url: string;
  controller: (req: Request, res: Response) => any;
}

export abstract class CustomRouter {
  name: string
  routes: Route[];
}
