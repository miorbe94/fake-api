abstract class Provider {
  abstract run(): Promise<void>;
}

export {
  Provider,
};
